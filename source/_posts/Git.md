title: Git只获取部分目录的内容（稀疏检出）

date: 2018-09-13 01:05

toc: true

---


## 背景

SVN和GIT文件存储的差异：

- SVN是基于文件方式的集中存储，支持从集中存储的库中拉取单个子文件

- Git却是基于元数据方式分布式存储文件信息的，它会在每一次Clone的时候将所有信息都取回到本地，即相当于在你的机器上生成一个克隆版的版本库。

因此在Git1.7.0以前，无法实现拉取单个文件或目录。但是幸运的是在Git1.7.0以后加入了Sparse Checkout模式，这使得Check Out指定文件或者文件夹成为可能。

## 打开 sparse checkout 功能

如果本地还没有建版本库，要用这个功能，先进入要放版本库的目录，在命令行执行几条命令：

```shell
git init <project>
cd <project>
git remote add origin ssh://<user>@<repository's url>
git config core.sparsecheckout true
echo "path1/" >> .git/info/sparse-checkout
echo "path2/" >> .git/info/sparse-checkout
git pull origin master
```

第一条命令`git init <project>`，先建立一个空的版本库，用实际的目录名替代

第二条命令`cd <project>`，进入创建的新的版本库的目录

第三条命令`git remote add origin ssh://<user>@<repository's url>`，添加远程库的地址

第四条命令`git config core.sparsecheckout true`，打开sparse checkout功能

第五第六条命令`echo "path1/" >> .git/info/sparse-checkout`，添加2个目录到checkout的列表。路径是版本库下的相对路径，也可以用文本编辑器编辑这个文件

第七条命令`git pull origin master`，拉取远程的 master 分支，也可以拉其他分支

如果只拉取最近一次的变更，忽略以前的变更记录，在拉取时可以加参数depth，如`git pull --depth=1 origin master` （浅克隆）

如果以后修改了 .git/info/sparse-checkout，增加或删除部分目录，可以执行如下命令重新Checkout

```shell
git checkout master
```

或执行以下命令：

```shell
git read-tree -mu HEAD
```

如果本地已经建了版本库，要使用这个功能，可以进入版本库的目录，执行以下命令

```shell
git config core.sparsecheckout true
echo "path1/" >> .git/info/sparse-checkout
echo "path2/" >> .git/info/sparse-checkout
git checkout master
```



要关闭 sparse checkout功 能，仅仅修改设置，将core.sparsecheckout设为false是不生效的，需要修改 .git/info/sparse-checkout 文件，用一个”*“号替代其中的内容，然后执行 checkout 或 read-tree 命令

## sparse-checkout 文件设置

**子目录的匹配**

在 sparse-checkout 文件中，如果目录名称前带斜杠，如`/docs/`，将只匹配项目根目录下的docs目录，如果目录名称前不带斜杠，如`docs/`，其他目录下如果也有这个名称的目录，如`test/docs/`也能被匹配。
而如果写了多级目录，如`docs/05/`，则不管前面是否带有斜杠，都只匹配项目根目录下的目录，如`test/docs/05/`不能被匹配。

**通配符 `*` (星号)**

在 sparse-checkout 文件中，支持通配符 `*`，如可以写成以下格式：

```shell
*docs/
index.*
*.gif
```

**排除项 `!`(感叹号)**

在 sparse-checkout 文件中，也支持排除项 `!`，如只想排除排除项目下的 “docs” 目录，可以按如下格式写：

```shell
/*
!/docs/
```

要注意一点：如果要关闭sparsecheckout功能，全取整个项目库，可以写一个`*`号，但如果有排除项，必须写”/*“，同时排除项要写在通配符后面

## 实践结果

test工程目录如下：

```shell
find . -print | grep -v .git | sed -e 's;[^/]*/;|____;g;s;____|; |;g'
|____tt2
| |____path.txt
| |____.DS_Store
| |____down
| | |____xx.md
| |____tmp
| | |____t2.txt
|____.DS_Store
|____tt3
| |____path.txt
| |____p3.md
| |____tmp
| | |____t3.txt
| | |____d3
| | | |____d3.txt
|____README.md
|____1.txt
```

### 实例1-取制定目录

取tt3目录

```shell
cd /Users/comma7456/CloudStation/Git
rm -rf test2
git init test2												## 建立本地仓库
cd test2
git config core.sparsecheckout true							## 打开sparsecheckout
git remote add origin git@gitlab.capd.net:jiacy/test.git	## 增加远程仓库
echo 'tt3' >> .git/info/sparse-checkout						## 配置文件sparse-checkout增加tt3规则
git pull origin master										##拉取代码
```

结果：

```shell
[comma7456@~/CloudStation/Git/test2]$find . -print | grep -v .git | sed -e 's;[^/]*/;|____;g;s;____|; |;g'
.
|____tt3
| |____path.txt
| |____p3.md
| |____tmp
| | |____t3.txt
| | |____d3
| | | |____d3.txt
```

### 实例2-取制定目录之外的其他目录

取除tt3目录之外的其他目录

```shell
cd /Users/comma7456/CloudStation/Git
rm -rf test2
git init test2												## 建立本地仓库
cd test2
git config core.sparsecheckout true							## 打开sparsecheckout
git remote add origin git@gitlab.capd.net:jiacy/test.git	## 增加远程仓库
echo '*' >> .git/info/sparse-checkout						## 配置*取全部代码
echo '!tt3/**' >> .git/info/sparse-checkout					## 排除tt3目录，/**表示tt3下目录
git pull origin master										##拉取代码
```

结果：

```shell
[comma7456@~/CloudStation/Git/test2]$find . -print | grep -v .git | sed -e 's;[^/]*/;|____;g;s;____|; |;g'
.
|____tt2
| |____path.txt
| |____down
| | |____xx.md
| |____tmp
| | |____t2.txt
|____README.md
|____1.txt
```



## 参考

[Git只获取部分目录的内容（稀疏检出）](https://zhgcao.github.io/2016/05/11/git-sparse-checkout/)

